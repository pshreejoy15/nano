#! /bin/sh

function clone_ota() {
    git clone https://gitlab.com/pshreejoy15/ota /home/rof/src/github.com/shreejoy/Nano_rosy/OTA
}

function get_value() {
    number=$(jq -r '.ver' /home/rof/src/github.com/shreejoy/Nano_rosy/OTA/rosy.json)
}

function increment() {
    increment=$[$number+1]
}

function new_values() {
    new_ver=$increment
    new_name="Nano-Beta-v$increment"
}

function new_json() {
    if [ -f /home/rof/src/github.com/shreejoy/Nano_rosy/OTA/$FILE_NAME_MIUI ]; then
    rm -rf /home/rof/src/github.com/shreejoy/Nano_rosy/OTA/rosy.json
    touch /home/rof/src/github.com/shreejoy/Nano_rosy/OTA/rosy.json
    echo "{\n   \"name\": \"$new_name\",\n   \"ver\": $new_ver,\n   \"url\": \"$LINK\"\n   \"miui_url\": \"$LINK_MIUI\"\n}" > /home/rof/src/github.com/shreejoy/Nano_rosy/OTA/rosy.json
    fi
}
    
function gitlab() {
    git config --global user.name "OTA_bot"
    git config --global user.email "rosycustomroms@gmail.com"
    cd /home/rof/src/github.com/shreejoy/Nano_rosy/OTA
    git add rosy.json
    git commit -m "New Update ($DATE_TIME)"
    git push 
}

function done() {
    echo -e "****************************************************";
    echo -e "* 888b    888        d8888 888b    888   d88888b   *";
    echo -e "* 8888b   888       d88888 8888b   888 d88P   Y88b *";
    echo -e "* 88888b  888      d88P888 88888b  888 888     888 *";
    echo -e "* 888Y88b 888     d88P 888 888Y88b 888 888     888 *";
    echo -e "* 888 Y88b888    d88P  888 888 Y88b888 888     888 *";
    echo -e "* 888  Y88888   d88P   888 888  Y88888 888     888 *";
    echo -e "* 888   Y8888  d8888888888 888   Y8888 Y88b   d88P *";
    echo -e "* 888    Y888 d88P     888 888    Y888   Y88888P   *";
    echo -e "****************************************************";    
}

clone_ota
get_value
increment
new_values
new_json
gitlab
done
