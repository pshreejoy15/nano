import json
from glob import glob
from os import environ

from requests import post

# telegram variables
bottoken = environ['bottoken']
telegram_chat = "@XiaomiChannelPro"

link = environ['LINK'] 
version = environ['bottoken'] 
filename = environ['bottoken'] 
time = environ['DIFF'] 

# load  json files
for file in glob('*.json'):
    with open(file) as f:
        info = json.load(f)
    # parse the json into telegram message
    data = []
    data.append('🔌 New Build\n\n')
    data.append('    ▪ File Name - {}\n'.format(filename))
    data.append('    ▪ Version - {}\n'.format(version))
    data.append('    ▪ Type - {}\n'.format(type))
    data.append('    ▪ Date/Time - {}\n\n\n'.format(time))
    data.append('Mirror - [Here]{}\n\n'.format(link))
    
    # remove empty entries
    for i in data:
        if ': \n' in i or '()' in i:
            data.remove(i)
    # create the message
    caption = ''.join(data)
    
    file = filename
    files = {
        'chat_id': (None, chat),
        'caption': (None, caption),
        'parse_mode': (None, mode),
        'disable_notification': (None, silent),
        'document': (file, open(file, 'rb')),
    }
    url = "https://api.telegram.org/bot" + token + "/sendDocument"
    r = post(url, files=files)
    # post to telegram
    telegram_req = post(url, files=files)
    status = telegram_req.status_code
    response = telegram_req.reason
    if status == 200:
        print("Message sent")
    else:
        print("Error: " + response)