#! /bin/sh
#
# Copyright (C) 2019 Raphielscape LLC.
#
# Licensed under the Raphielscape Public License, Version 1.0 (the "License");
# you may not use this file except in compliance with the License.
#
# Kernel building script.

sudo chmod -R 777 /home/rof/src/github.com/shreejoy/Nano_rosy/scripts/fetch-latest-wireguard.sh


KERNEL_DIR=`pwd`
function colors {
	blue='\033[0;34m' cyan='\033[0;36m'
	yellow='\033[0;33m'
	red='\033[0;31m'
	nocol='\033[0m'
}

colors;
PARSE_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
PARSE_ORIGIN="$(git config --get remote.origin.url)"
COMMIT_POINT="$(git log --pretty=format:'%h : %s' -1)"

TELEGRAM_TOKEN=${BOT_API_KEY}
export BOT_API_KEY PARSE_BRANCH PARSE_ORIGIN COMMIT_POINT TELEGRAM_TOKEN
. "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/stacks/telegram
kickstart_pub
function clone {
	git clone --depth=1 --no-single-branch https://github.com/shreejoy/AnyKernel2 /home/rof/src/github.com/shreejoy/Nano_rosy/anykernel2
	git clone --depth=1 --no-single-branch https://github.com/mscalindt/aarch64-linux-android-4.9 /home/rof/src/github.com/shreejoy/Nano_rosy/Toolchain
   }

function exports {
	export KBUILD_BUILD_USER="shreejoy"
	export KBUILD_BUILD_HOST="nano-ci"
	export ARCH=arm64
	export SUBARCH=arm64
        PATH=/home/rof/src/github.com/shreejoy/Nano_rosy/Toolchain/bin:$PATH
	export PATH
}



function build_kernel {
	#better checking defconfig at first
	if [ -f $KERNEL_DIR/arch/arm64/configs/rosy_defconfig ]
	then 
		DEFCONFIG=rosy_defconfig
		export TYPE="AOSP"
		export SF_PATH="nano-releases/AOSP"
		export NUM="5"
		export BUILD_DATE="$(date +%M%Y%H-%d%m)"
		export FILE_NAME="Nano_Kernel-rosy-$BUILD_DATE-$TYPE-v$NUM.zip"
		export LINK="https://master.dl.sourceforge.net/project/$SF_PATH/$FILE_NAME"

	else
		echo "Defconfig Mismatch"
		echo "Exiting in 5 seconds"
		sleep 5
		exit
	fi
	
CPU="$(grep -c '^processor' /proc/cpuinfo)"
JOBS="$((CPU * 2))"

	make O=out $DEFCONFIG
	BUILD_START=$(date +"%s")
	make -j${JOBS} O=out \
	CROSS_COMPILE="/home/rof/src/github.com/shreejoy/Nano_rosy/Toolchain/bin/aarch64-linux-android-" 
	BUILD_END=$(date +"%s")
	BUILD_TIME=$(date +"%Y%m%d-%T")
	DIFF=$((BUILD_END - BUILD_START))	
}

function build_kernel_miui {
	#better checking defconfig at first
	if [ -f $KERNEL_DIR/arch/arm64/configs/rosy-miui_defconfig ]
	then 
		DEFCONFIG=rosy-miui_defconfig
		export TYPE="MIUI"
		export SF_PATH_MIUI="nano-releases/MIUI"
		export NUM="5"
		export BUILD_DATE="$(date +%M%Y%H-%d%m)"
		export FILE_NAME_MIUI="Nano_Kernel-rosy-$BUILD_DATE-$TYPE-v$NUM.zip"
		export LINK_MIUI="https://master.dl.sourceforge.net/project/$SF_PATH_MIUI/$FILE_NAME_MIUI"

	else
		echo "Defconfig Mismatch"
		echo "Exiting in 5 seconds"
		sleep 5
		exit
	fi
	
CPU="$(grep -c '^processor' /proc/cpuinfo)"
JOBS="$((CPU * 2))"

	mkdir -p out-miui
	make O=out-miui $DEFCONFIG
	BUILD_START=$(date +"%s")
	make -j${JOBS} O=out-miui \
	CROSS_COMPILE="/home/rof/src/github.com/shreejoy/Nano_rosy/Toolchain/bin/aarch64-linux-android-" 
	BUILD_END=$(date +"%s")
	BUILD_TIME=$(date +"%Y%m%d-%T")
	DIFF=$((BUILD_END - BUILD_START))	
}

function check_img {
	if [ -f /home/rof/src/github.com/shreejoy/Nano_rosy/out/arch/arm64/boot/Image.gz-dtb ]
	then 
		echo -e "Kernel Built Successfully in $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds..!!"
		gen_zip
	else 
		finerr
	fi	
}

function check_img_miui {
	if [ -f /home/rof/src/github.com/shreejoy/Nano_rosy/out-miui/arch/arm64/boot/Image.gz-dtb ]
	then 
		echo -e "MIUI Kernel Built Successfully in $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds..!!"
		gen_zip_miui
	else 
		finerr_miui
	fi	
}

function gen_zip {
	if [ -f $KERNEL_DIR/out/arch/arm64/boot/Image.gz-dtb ]
	then 
		echo "Zipping Files.."
		mv /home/rof/src/github.com/shreejoy/Nano_rosy/out/arch/arm64/boot/Image.gz-dtb anykernel2/Image.gz-dtb
		mv /home/rof/src/github.com/shreejoy/Nano_rosy/anykernel2/Image.gz-dtb /home/rof/src/github.com/shreejoy/Nano_rosy/anykernel2/zImage
		cd /home/rof/src/github.com/shreejoy/Nano_rosy/anykernel2
		zip -r $FILE_NAME * -x .git README.md 
		fin
		rm -rf zImage
		mv $FILE_NAME /home/rof/src/github.com/shreejoy/Nano_rosy
		cd ..
        fi
}

function gen_zip_miui {
	if [ -f $KERNEL_DIR/out-miui/arch/arm64/boot/Image.gz-dtb ]
	then 
		echo "Zipping Files.."
		mv /home/rof/src/github.com/shreejoy/Nano_rosy/out-miui/arch/arm64/boot/Image.gz-dtb anykernel2/Image.gz-dtb
		mv /home/rof/src/github.com/shreejoy/Nano_rosy/anykernel2/Image.gz-dtb /home/rof/src/github.com/shreejoy/Nano_rosy/anykernel2/zImage
		cd /home/rof/src/github.com/shreejoy/Nano_rosy/anykernel2
		zip -r $FILE_NAME_MIUI * -x .git README.md 
		fin_miui
		rm -rf zImage
		mv $FILE_NAME_MIUI /home/rof/src/github.com/shreejoy/Nano_rosy
		cd ..
        fi
}
tg_senderror() {
    tg_debugcast "‼Build Throwing Error(s)" \
    "@AndroidPie9 check the errors 🛑 "
     exit 1
}

tg_yay() {
    tg_sendinfo "✅ Build successfully completed"
}

# Fin Prober
fin() {
    echo "Nano kernel AOSP build completed in $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds.~"
    tg_sendinfo "Nano AOSP kernel build completed 😃 in $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds"
    tg_yay
}
finerr() {
    echo "My works took $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds but it's error..."
    tg_debugcast "AOSP kernel build for rosy took $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds" \
                " " \
                "❌ but it's having some error "
    tg_senderror
    exit 1
}

fin_miui() {
    echo "Nano kernel MIUI build completed in $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds.~"
    tg_sendinfo "Nano MIUI kernel build completed 😃 in $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds"
    tg_yay
}
finerr_miui() {
    echo "My works took $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds but it's error..."
    tg_debugcast "MIUI kernel build for rosy took $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds" \
                " " \
                "❌ but it's having some error "
    tg_senderror
    exit 1
}

function ota_time() {
    cd /home/rof/src/github.com/shreejoy/Nano_rosy/nano
    ./generate_ota.sh
}

function push {
        cd /home/rof/src/github.com/shreejoy/Nano_rosy
        tg_build_sendsticker
        scp $FILE_NAME pshreejoy15@frs.sourceforge.net:/home/frs/p/$SF_PATH
        scp $FILE_NAME_MIUI pshreejoy15@frs.sourceforge.net:/home/frs/p/$SF_PATH_MIUI
        tg_channelcast "👇🏻 Made for AOSP Roms"
        tg_channelcast "Download Mirror 👉🏻 $LINK"
        curl -F chat_id="-1001373366892" -F document=@"$FILE_NAME" https://api.telegram.org/bot$BOT_API_KEY/sendDocument
        tg_channelcast "👇🏻 Made for MIUI Roms"
        curl -F chat_id="-1001373366892" -F document=@"$FILE_NAME_MIUI" https://api.telegram.org/bot$BOT_API_KEY/sendDocument
        tg_channelcast "Download Mirror 👉🏻 $LINK_MIUI" 
        note
        ota_time
	}

clone
exports
build_kernel
check_img
build_kernel_miui
check_img_miui
push
